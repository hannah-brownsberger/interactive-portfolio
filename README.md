## Hannah Brownsberger

**Welcome to my professional Bitbucket account! On this interactive portfolio, I will display the many skills I have learned while pursuing my B.S. in Information Technology. I am currently located in the Floirda Panhandle but I plan to move after graduatation and hope my career will involve travel.**

**As a senior student at Florida State University, I am pursuing a degree in Information Technology with an elective focus in Project Managament, Information Architecture, and UI/UX Design.**

*Below, you will find links to my most notable accomplishments, links, and projects that I have completed while at Florida State Univeristy.*

1. [Resume (last updated 12/02/21)](https://www.canva.com/design/DAExbQ71YgU/Eb9DGFoHH8hiiKGP_0f_1g/view?utm_content=DAExbQ71YgU&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton)
    * Clicking this link will send you to a printable screen of my most recent resume made on Canva.

2. [LinkedIn](https://www.linkedin.com/in/hannah-brownsberger/)
    * This link will take you to my LinkedIn page where you can see my skills, accomplishments, memberships, and more. Feel free to connect with me as I would love to talk to you!

3. [Baba Yaga's Bits and Bobs](https://www.babayagasbitsandbobs.com/)
    * This is my small business that I began in 2019. It has grown tremendously considering it began as me merely posting on instagram about some neat dice that I made. Now, it is part of my daily life. Visit my website to learn more about it and to visit my interactive resume!

4. [LIS 4381 (Mobile Web Application Development):](https://bitbucket.org/hannah-brownsberger/lis4381/src/master/)
    * *Course Description:*
    * This course focuses on concepts and best practices for developing and managing “mobile-first” technology projects. It covers processes and requirements for developing mobile web applications and principles for effective interface and user experience design. Students will also examine different issues and concerns that may influence the widespread adoption and implementation of mobile web applications.

5. [LIS 4910 (Information Technology Project):](https://bitbucket.org/hannah-brownsberger/lis4910/src/master/)
    * *Course Description:*
    * Students work in teams or individually to manage, design, implement and evaluate an information technology project. Students are also given evaluation and guidance on improving artifacts from projects entered into their degree portfolio during this and other courses within the degree program.

6. [LIS 4480 (Information Technology Leadership):](https://bitbucket.org/hannah-brownsberger/lis4480/src/master/)
    * *Course Description:*
    * This course focuses on leadership, group communication, project planning, strategy, and individual development, with a focus on Information Technology and its uses. Students participating in this class actively design, implement, and coordinate numerous ongoing projects that build a strong team atmosphere and allow students to gain valuable leadership, communication, and organizational skills within the context of contemporary IT organizations. May be repeated to a maximum of six semester hours; duplicate registration not allowed.

7. [Florida State University Marching Chiefs](https://www.wtxl.com/news/local-news/fsu-marching-chiefs-head-to-france-for-d-day-parade-performance)
    * I was a proud member of the FSU Marching Chiefs from August 2018 to January 2020. While I may not have been in the football stands the last two years, I will forever be a Marching Chief. MCATDT!




> If you have any questions or would like to speak with me directly, please contact me via email at hbrownsberger2@gmail.com or via text at 8504617363.
>
> [Linktr.ee](https://linktr.ee/hannahbrownsberger)
>